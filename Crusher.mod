<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="Crusher" version="1.0.10" date="2010/12">
		<Author name="Wikki" email="wikkifizzle@gmail.com" />
		<Description text="Crusher - No relation to Beverly or Wesley" />
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="EA_BackpackWindow" />
			<Dependency name="EA_CraftingSystem" />
			<Dependency name="EASystem_Strings" />
			<Dependency name="EASystem_Utils" />
			<Dependency name="EASystem_ResourceFrames" />
			<Dependency name="EASystem_WindowUtils" />
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
        </Dependencies>
        
        <SavedVariables>
			<SavedVariable name="CrusherDB" global="true" />
		</SavedVariables>
		
		<Files>
			<File name="Assets/Assets.xml" />
		
			<File name="Libraries/LibStub.lua" />
			<File name="Libraries/LibGUI.lua" />
			<File name="Libraries/AceLocale-3.0.lua" />
			<File name="Libraries/AceDB-3.0.lua" />
			<File name="Libraries/AceDBOptions-3.0.lua" />
			<File name="Libraries/CallbackHandler-1.0.lua" />
			
			<File name="Localization/deDE.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Localization/esES.lua" />
			<File name="Localization/frFR.lua" />
			<File name="Localization/itIT.lua" />
			<File name="Localization/jaJP.lua" />
			<File name="Localization/koKR.lua" />
			<File name="Localization/ruRU.lua" />
			<File name="Localization/zhCN.lua" />
			<File name="Localization/zhTW.lua" />
			
			<!-- Start Configuration Windows -->
			<File name="Configuration/Config.xml" />
			<File name="Configuration/Config.lua" />
			<File name="Configuration/Config_General.lua" />
			<File name="Configuration/Config_Profiles.lua" />
			<!-- End Configuration Windows -->
			
			<File name="Source/Crusher.lua" />
			<File name="Source/Crusher.xml" />
		</Files>

		<OnUpdate>
			<CallFunction name="Crusher.OnUpdate"/>
		</OnUpdate>
		<OnInitialize>
			<CallFunction name="Crusher.OnInitialize" />
			<CreateWindow name="CrusherConfig" show="false" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="AceDBLogoutHandler" />
        </OnShutdown>
		
		<WARInfo>
		    <Categories>
		        <Category name="ITEMS_INVENTORY" />
		        <Category name="CRAFTING" />
		    </Categories>
		    <Careers>
		        <Career name="BLACKGUARD" />
		        <Career name="WITCH_ELF" />
		        <Career name="DISCIPLE" />
		        <Career name="SORCERER" />
		        <Career name="IRON_BREAKER" />
		        <Career name="SLAYER" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="ENGINEER" />
		        <Career name="BLACK_ORC" />
		        <Career name="CHOPPA" />
		        <Career name="SHAMAN" />
		        <Career name="SQUIG_HERDER" />
		        <Career name="WITCH_HUNTER" />
		        <Career name="KNIGHT" />
		        <Career name="BRIGHT_WIZARD" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="CHOSEN" />
		        <Career name= "MARAUDER" />
		        <Career name="ZEALOT" />
		        <Career name="MAGUS" />
		        <Career name="SWORDMASTER" />
		        <Career name="SHADOW_WARRIOR" />
		        <Career name="WHITE_LION" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>
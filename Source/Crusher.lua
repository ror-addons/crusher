--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if Crusher == nil then Crusher = {} end 

local Crusher 		= Crusher
local LibGUI 		= LibStub( "LibGUI" )
if( LibGUI == nil ) then d( "(Crusher) - Error loading LibGUI") return end

local pairs 		= pairs
local ipairs		= ipairs
local tonumber 		= tonumber
local tostring 		= tostring
local towstring 	= towstring
local tinsert		= table.insert
local tsort			= table.sort
local string_format = string.format

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]

local T 				= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Crusher", debug )

local VERSION 			= { MAJOR = 1, MINOR = 0, REV = 10 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 		= DisplayVersion .. " Dev"
else
	DisplayVersion 		= DisplayVersion .. " r" .. ( tonumber( "30" ) or "0" )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

do
	function Crusher.wipe(t)
		for k,_ in pairs( t )
		do
			t[k] = nil
		end 	
	end

	local cache = setmetatable({}, {__mode='k'})
	
	--- Return a table
	-- @usage local t = Crusher.new()
	-- @return a blank table
	function Crusher.new()
		local t = next(cache)
		if t then
			cache[t] = nil
			return t
		end
		
		return {}
	end
	
	local wipe = Crusher.wipe
	
	--- Delete a table, clearing it and putting it back into the queue
	-- @usage local t = Crusher.new()
	-- t = del(t)
	-- @return nil
	function Crusher.del(t)
		wipe(t)
		cache[t] = true
		return nil
	end
end

local function ripairs(t)
  local max = 1
  while t[max] ~= nil do
    max = max + 1
  end
  local function ripairs_it(t, i)
    i = i-1
    local v = t[i]
    if v ~= nil then
      return i,v
    else
      return nil
    end
  end
  return ripairs_it, t, max
end

local new, del, wipe = Crusher.new, Crusher.del, Crusher.wipe

local db
local dbOptions

local defaultSettings 	=
{
	profile = 
	{
		anchor =
		{
			point					= "center",
			relpoint				= "center",
			relwin					= "Root",
			x						= 0,
			y						= 0,
			scale					= 1.0,
		},
		
		general =
		{
			debug					= false,
			version					= VERSION,
			show					= true,
			stopincombat			= true,
			stoponspellcast			= true,
		},
		
		icon = 
		{
			point					= "center",
			relpoint				= "center",
			relwin					= "Root",
			x						= 0,
			y						= 0,
			scale					= .75,
			visible					= true,
		},
	},
	
	faction = 
	{
		stats = {},
	}
}

local windowId						= "CrusherWindow"
local configWindowId				= "CrusherConfig"
local iconWindowId					= "CrusherIcon"
local firstLoad						= true
local activeItemData				= nil
local activeSlot					= nil
local activeLoc						= nil
local activeStatMenuLookup			= new()
local crushCount					= 0
local isRunning						= false
local playerInventoryUpdateReceived	= false
local inventoryUpdateThrottle		= 0
local tooltipWindow					= nil

local goColor						= { 0, 200, 0 }
local stopColor						= { 200, 0, 0 }
local extraTextColor				= { r=0, g=200, b=0 }

local getTotalItemCount, initializeUI, setActiveItemData
local updateUIPosition, updateTotalItemCountDisplay
local isItemSalvagable, isItemSalvagableByPlayer
local continueCrushing, displayStatSelectionMenu, getAllAvailableSlotsForItem
local performCrush, isCrushableStat

Crusher.itemLocToBackpackType =
{
	[GameData.ItemLocs.INVENTORY] 		= EA_Window_Backpack.TYPE_INVENTORY,
	[GameData.ItemLocs.CRAFTING_ITEM] 	= EA_Window_Backpack.TYPE_CRAFTING,
}

------------------------------------------------
-- Runtime Variables
------------------------------------------------

local window	= {
	W			= {},
	I			= {},
}

function Crusher.GetVersion() return DisplayVersion end

function Crusher.OnInitialize()
	-- Create our configuration objects
	db				= LibStub( "WAR-AceDB-3.0" ) : New( "CrusherDB", defaultSettings, "Default" )
	dbOptions		= LibStub( "WAR-AceDBOptions-3.0" ) : GetOptionsTable( db )
	dbOptions.args 	= {} -- We do not use these args, so free up the mem
	defaultSettings = nil
	
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "Crusher.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "Crusher.OnLoad" )
end

function Crusher.OnLoad()
	Crusher.Debug( "Crusher.OnLoad" )
	
	if( firstLoad ) then
		-- Register a callback to know if the profile changes
		db.RegisterCallback( Crusher, "OnProfileChanged", Crusher.OnProfileChanged )
		db.RegisterCallback( Crusher, "OnProfileCopied", Crusher.OnProfileChanged )
		db.RegisterCallback( Crusher, "OnProfileReset", Crusher.OnProfileChanged )
		
		-- Register our slash commands with LibSlash
		Crusher.Debug( "Registering slash commands" )
		
		-- Initialize our UI
		initializeUI()
		
		-- Catch the layout editor events
		LayoutEditor.RegisterEditCallback( Crusher.OnLayoutEditorEvent )
		
		-- Register the UI with the layout editor
		LayoutEditor.RegisterWindow( windowId, L"Crusher", L"Crusher", false, false, false, nil )
		LayoutEditor.RegisterWindow( iconWindowId, T["Crusher Toggle"], T["Crusher Toggle"], false, false, true, nil )
		
		-- Load our configuration window
		CrusherConfig.OnLoad( db, dbOptions )
		
		-- Print our initialization usage
		local init = WStringToString( T["Crusher %s initialized."] )
		Crusher.Print( init:format( DisplayVersion ) )
		
		-- Attempt to register one of our handlers
		if( LibSlash ~= nil and type( LibSlash.RegisterSlashCmd ) == "function" ) then
			LibSlash.RegisterSlashCmd( "crusher", Crusher.Slash )
			LibSlash.RegisterSlashCmd( "crush", Crusher.Slash )
			Crusher.Print( T["(Crusher) use /crusher to bring up the Crusher window."] )
		end
		
		RegisterEventHandler( SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, 	"Crusher.OnPlayerInventorySlotUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_CRAFTING_SLOT_UPDATED, 	"Crusher.OnPlayerCraftingSlotUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED,		"Crusher.OnPlayerCombatFlagUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_BEGIN_CAST, 				"Crusher.OnPlayerBeginCast" )
		RegisterEventHandler (SystemData.Events.PLAYER_END_CAST,				"Crusher.OnPlayerEndCast")
		
		d( "Crusher loaded." )
		
		firstLoad = false
	end
	
	--
	-- If the user zoned in the middle of crushing, stop crushing
	--
	if( isRunning ) then
		Crusher.StopCrushing()
	end
end

function Crusher.Debug(str)	
	if( db ~= nil and db.profile.general.debug ) then 
		DEBUG(towstring(str)) 
	end 
end

function Crusher.OnHidden()
	if( firstLoad ) then return end
	db.profile.general.show = false
end

function Crusher.OnLButtonDown_StatsMenuItem( idx )
	-- If we received valid information, store the stat for the item
	if( activeStatMenuLookup[idx] ~= nil ) then
		db.faction.stats[activeItemData.uniqueID] = activeStatMenuLookup[idx].stat
	end
end

function Crusher.OnLayoutEditorEvent( code )
	if( code == LayoutEditor.EDITING_END ) then
		-- Save our UI location
		db.profile.anchor.point, db.profile.anchor.relpoint, db.profile.anchor.relwin, db.profile.anchor.x, db.profile.anchor.y = WindowGetAnchor( windowId, 1 )
		db.profile.anchor.scale = WindowGetScale( windowId )
		
		db.profile.icon.point, db.profile.icon.relpoint, db.profile.icon.relwin, db.profile.icon.x, db.profile.icon.y = WindowGetAnchor( iconWindowId, 1 )
		db.profile.icon.scale = WindowGetScale( iconWindowId )
		db.profile.icon.visible = WindowGetShowing( iconWindowId )
	end
end

function Crusher.OnMouseOver_Icon()
	Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Show/Hide Crusher"] )
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
end

function Crusher.OnPlayerBeginCast( abilityId, isChannel, desiredCastTime, averageLatency )
	if( isRunning and abilityId ~= 0 and db.profile.general.stoponspellcast ) then
		Crusher.StopCrushing()
	end
end

function Crusher.OnPlayerCombatFlagUpdated()
	if( isRunning and db.profile.general.stopincombat and GameData.Player.inCombat ) then
		Crusher.Debug( "Crusher.OnPlayerCombatFlagUpdated" )
		Crusher.StopCrushing()
	end
end

function Crusher.OnPlayerEndCast( isCancel )
	if( isRunning and isCancel ) then
		Crusher.StopCrushing()
	end
end

function Crusher.OnPlayerCraftingSlotUpdated( updatedSlots )
 	playerInventoryUpdateReceived = true
 	
 	if( activeLoc == GameData.ItemLocs.CRAFTING_ITEM ) then
	 	for k, v in pairs( updatedSlots )
	 	do
	 		if( v == activeSlot ) then
	 			continueCrushing()
	 			break
	 		end
	 	end
 	end
end

function Crusher.OnPlayerInventorySlotUpdated( updatedSlots )
	playerInventoryUpdateReceived = true
	
	if( activeLoc == GameData.ItemLocs.INVENTORY ) then
	 	for k, v in pairs( updatedSlots )
	 	do
	 		if( v == activeSlot ) then
	 			continueCrushing()
	 			break
	 		end
	 	end
 	end
end

function Crusher.OnUpdate( elapsedTime )
	inventoryUpdateThrottle = inventoryUpdateThrottle + elapsedTime
	if( inventoryUpdateThrottle > 1 ) then
		if( playerInventoryUpdateReceived ) then
			updateTotalItemCountDisplay()
			playerInventoryUpdateReceived = false
		end
		inventoryUpdateThrottle = 0
	end
end

function Crusher.Print( wstr )
	EA_ChatWindow.Print( towstring( wstr ), ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id )
end

function Crusher.OnShown()
	if( firstLoad ) then return end
	db.profile.general.show = true
end

function Crusher.OnProfileChanged( eventName )
	local currentProfile	= dbOptions.handler:GetCurrentProfile()
	local wCurrentProfile	= dbOptions.handler:GetProfileDisplayName( currentProfile )
	local profile 			= wCurrentProfile or towstring( currentProfile )
	
	-- Stop crushing, just in case some setting changes that could be bad	
	Crusher.StopCrushing()
		
	-- Update our UIs position
	updateUIPosition()
	
	-- Rever the crusher dialog so it loads up the new settings
	CrusherConfig.RevertDialog()
	
	Crusher.Print( L"(Crusher) " .. T["Active Profile:"] .. L" '" .. profile .. L"'" )
end

function Crusher.StartCrushing()
	if( isRunning ) then return end
	
	-- Retrieve the crush count	
	crushCount = tonumber( window.W.General.CrushCount_Textbox:GetText() ) or 0
	
	-- Make sure we have some work to do
	if( crushCount == 0 ) then return end

	isRunning = true
	
	-- Start doing our work, if we suceed flag we are running and return
	if( performCrush() ) then
		-- Identify this button now as the stop button
		window.W.General.Crush_Button:Tint( unpack( stopColor ) )
		return 
	end
	
	isRunning = false
	
	-- If we made it here, stop crafting
	Crusher.StopCrushing()
end

function Crusher.StopCrushing()
	if( not isRunning ) then return end
	
	-- Identify this button now as the go button
	window.W.General.Crush_Button:Tint( unpack( goColor ) )
	
	-- Release our locks
	EA_BackpackUtilsMediator.ReleaseAllLocksForWindow( windowId )
	
	-- Set the make count to 1
	crushCount = 0
	
	-- Clear our active loc and slot
	activeLoc = nil
	activeSlot = nil
	
	-- Update our make count display
	window.W.General.CrushCount_Textbox:SetText( towstring( crushCount ) )
	
	isRunning = false
end

function Crusher.Slash( input )
	if( not isRunning ) then Crusher.Toggle() end
end

function Crusher.Toggle()
	local show = not WindowGetShowing( windowId )
	WindowSetShowing( windowId, show ) 
end

function Crusher.ToggleConfig()
	if( CrusherConfig ~= nil and DoesWindowExist( CrusherConfig.GetWindowName() ) ) then
		WindowSetShowing( CrusherConfig.GetWindowName(), not WindowGetShowing( CrusherConfig.GetWindowName() ) )
	end
end

------------------------------------------------------------
-- LOCAL FUNCTIONS
------------------------------------------------------------
function continueCrushing()
	Crusher.Debug( "Entering continueCrushing" )
	
	-- Release our locks
	EA_BackpackUtilsMediator.ReleaseAllLocksForWindow( windowId )
	
	-- Reduce the make count	
	crushCount = crushCount - 1
	
	-- Update our count display
	window.W.General.CrushCount_Textbox:SetText( towstring( crushCount ) )
	
	-- If we have more work to do attempt to do it	
	if( crushCount > 0 ) then
		if( performCrush() ) then
			return 
		end
	end
	
	-- If we made it here, stop crafting
	Crusher.StopCrushing()
end

function displayStatSelectionMenu()
	if( activeItemData == nil ) then return end
	
	-- Clear any current lookup information
	for _, v in pairs( activeStatMenuLookup )
	do
		del(v)
	end
	wipe( activeStatMenuLookup )
	
	-- Create a list of stats to display to the user
	local bonusAdded = false
	for index, bonus in ipairs( activeItemData.bonus )
    do
    	local ref = bonus.reference
    	if( CraftingUtils.SalvagingStatStringLookUp[ref] ~= nil ) then	
			local menuEntry = new()
			menuEntry.stat 		= ref
			menuEntry.disp 		= L"   " .. BonusTypes[ref].name
			tinsert( activeStatMenuLookup, menuEntry )
			bonusAdded = true
		end
    end
    
    -- If we have stats in our lookup, sort and display the menu
    if( bonusAdded ) then
    	local function sort( a, b ) 
			return a.disp < b.disp
		end
		
		-- Sort the menu based off of the stat name
		tsort( activeStatMenuLookup, sort ) 
		
		-- Build and display the menu
		EA_Window_ContextMenu.CreateContextMenu( windowId, nil, T["Stat Selection"] )
		for k, v in ipairs( activeStatMenuLookup )
		do
			EA_Window_ContextMenu.AddMenuItem( v.disp, 
				( function()
					Crusher.OnLButtonDown_StatsMenuItem(k)
				end ), false, true )
		end
		EA_Window_ContextMenu.Finalize( nil, { Point = "topleft", RelativePoint = "topright", RelativeTo = windowId, XOffset = 0, YOffset = 0 } )
	else
		-- There were no stats on the item so we use an active stat of zero
		db.faction.stats[activeItemData.uniqueID] = 0
	end 
end

function getAllAvailableSlotsForItem( itemData )
	local foundItems = new()
	
	local itemSets = new()
	tinsert( itemSets, { loc = GameData.ItemLocs.INVENTORY, data=DataUtils.GetItems() } )
	tinsert( itemSets, { loc = GameData.ItemLocs.CRAFTING_ITEM, data=DataUtils.GetCraftingItems() } )
	
	for _, set in ipairs( itemSets )
	do
		for slot, item in ipairs( set.data )
		do
			if( itemData.uniqueID == item.uniqueID ) then
				local foundItem = {}
				foundItem.loc	= set.loc
				foundItem.slot 	= slot
				foundItem.stackCount = item.stackCount
				table.insert( foundItems, foundItem )
			end
		end
	end
	
	del(itemSets)

	return foundItems
end

function getTotalItemCount( itemData )
	if( itemData == nil ) then return 0 end
	
	local itemSets = new()
	tinsert( itemSets, { loc = GameData.ItemLocs.INVENTORY, data=DataUtils.GetItems() } )
	tinsert( itemSets, { loc = GameData.ItemLocs.CRAFTING_ITEM, data=DataUtils.GetCraftingItems() } )

	local count	= 0
	
	for _, set in ipairs( itemSets )
	do
		for slot, item in ipairs( set.data )
		do
			if( itemData.uniqueID == item.uniqueID ) then
				count = count + item.stackCount
			end
		end
	end
	
	del(itemSets)
	
	return count
end

function initializeUI()
	local w
	local e
	
	------------------------------
	-- ICON
	------------------------------
	
	-- General Window
	w = LibGUI( "window", iconWindowId, "CrusherIconBase" )
	w:ClearAnchors()
	w:Resize( 38, 38 )
	w:Show()
	w.OnMouseOver = Crusher.OnMouseOver_Icon
	window.I.General = w
	
	------------------------------
	-- MAIN UI
	------------------------------
	
	-- General Window
	w = LibGUI( "window", windowId, "CrusherConfigBase" )
	w:Resize( 76, 135 )
	if( db.profile.general.show ) then
		w:Show()
	else
		w:Hide()
	end
	window.W.General = w
	
	-- General - Item Button
    e = window.W.General( "Button", windowId .. "ItemButton", "Crusher_Button_DefaultIconFrame" )
    e:Resize( 64, 64 )
    e:AnchorTo( window.W.General, "topleft", "topleft", 6, 5 )
    e:AnchorTo( window.W.General, "topright", "topright", -6, 5 )
    e:Layer( "secondary" )
    e.OnRButtonUp =
    	function()
    		-- If we are not running, clear the active item data
    		if( not isRunning ) then
    			-- Hide the tooltip if its displayed
    			if( tooltipWindow ~= nil and DoesWindowExist( tooltipWindow ) ) then
					WindowSetShowing( tooltipWindow, false )
				end
					
    			-- Clear the active item
    			setActiveItemData( nil )
    		end
    	end
    e.OnLButtonUp = 
    	function()
    		-- Only allow interaction here if we are not running
    		if( not isRunning ) then 
    			-- If the user is dragging the appropriate item, start using that item
				if( Cursor.IconOnCursor() and Cursor.Data ~= nil and 
					( Cursor.Data.Source == Cursor.SOURCE_INVENTORY or Cursor.Data.Source == Cursor.SOURCE_CRAFTING_ITEM ) and 
					Cursor.Data.SourceSlot ~= nil ) then
					
					-- Get the item data from the slot
					local itemData = DataUtils.GetItemData( Cursor.Data.Source, Cursor.Data.SourceSlot )
					
					-- Set the active item data to the given item
					setActiveItemData( itemData )
					
					-- Clear cursor
					Cursor.Clear()
				elseif( activeItemData ~= nil ) then
					-- The user did not have an appropriate item, however, we
					-- already have an item, so display the stat selection menu
					-- to the user
					
					-- If an item tooltip is displaying, hide it
					if( tooltipWindow ~= nil and DoesWindowExist( tooltipWindow ) ) then
						WindowSetShowing( tooltipWindow, false )
					end
					
					-- Clear cursor
					Cursor.Clear()
				
					-- Display the stat selection menu
					displayStatSelectionMenu()
				end
			end
    	end
    e.OnMouseOver =
    	function()
    		if( not Cursor.IconOnCursor() ) then
	    		if( activeItemData ~= nil ) then
	    			local extraText = L""
					
					local stat = db.faction.stats[activeItemData.uniqueID]
					
					-- If the user has selected a stat add it to the tooltip
	    			if( stat ~= nil and BonusTypes[stat] ~= nil ) then
	    				extraText = T["Crusher Stat:"] .. L"  " .. BonusTypes[stat].name
	    			end
	    			
	    			tooltipWindow = Tooltips.CreateItemTooltip( activeItemData, SystemData.MouseOverWindow.name, Tooltips.ANCHOR_WINDOW_VARIABLE, true, extraText, extraTextColor, true )
	            else
	            	Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Drag an item here to crush it."] )
	            	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_VARIABLE)
				end
			end
    	end
    window.W.General.Item_Button = e
    
    e = window.W.General( "Button", windowId .. "ItemButtonOverlay", "Crusher_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Item_Button.name ) )
    e:AnchorTo( window.W.General.Item_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.ItemOverlay_Button = e
        
    -- General - Crush Count Textbox
    e = window.W.General( "Textbox", windowId .. "MakeCountTextBox", "Crusher_EditBox_DefaultFrame" )
    e:Resize( 65, 32 )
    e:Show()
    e:ClearAnchors()
    e:AddAnchor( window.W.General.Item_Button, "bottom", "top", 0, 1 )
    e:Layer( "secondary" )
    e:SetText( 0 )
    window.W.General.CrushCount_Textbox = e
    
    -- General - Crush Button
    e = window.W.General( "Button", windowId .. "CrushButton", "Crusher_Button_Crush" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.CrushCount_Textbox, "bottomleft", "topleft", 4, 2 )
    e:Layer( "secondary" )
    e.OnLButtonUp = 
    	function()
    		if( isRunning ) then
    			Crusher.StopCrushing()
    		else
    			Crusher.StartCrushing()
    		end
    	end
    e.OnMouseOver =
    	function()
    		local display
    		if( isRunning ) then
    			display = T["Stop Crushing Item(s)"]
    		else
    			display = T["Start Crushing Item(s)"]
    		end
    	
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, display )
    		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    e:Tint( unpack( goColor ) )
    window.W.General.Crush_Button = e
    
    e = window.W.General( "Button", windowId .. "ConfigButton", "Crusher_Button_Config" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.CrushCount_Textbox, "bottomright", "topright", -4, 2 )
    e:Layer( "secondary" )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Toggle Configuration"] )
    		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    e.OnLButtonUp = 
    	function()
    		Crusher.ToggleConfig()
    	end
    window.W.General.Config_Button = e
    
    updateUIPosition()
end

function isCrushableStat( bonus )
	return STATS[bonus.reference] ~= nil
end

function isItemSalvagable( itemData )
	if( itemData ~= nil ) then
		return itemData.flags[GameData.Item.EITEMFLAG_MAGICAL_SALVAGABLE] == true
	end
	return false 
end

function isItemSalvagableByPlayer( itemData )
	if( alpha ) then return true end
	
	-- Sanity check the item and see if the item itself is salvagable
	if( itemData ~= nil and isItemSalvagable( itemData ) ) then
		-- Check if the player actually has the salvage skill
		if( GetTradeSkillLevel( GameData.TradeSkills.SALVAGING ) > 0 ) then
			return GameData.Salvaging.IMPOSSIBLE ~= GetSalvagingDifficulty( itemData.iLevel )
		end
	end
	
	return false
end 	

function performCrush()
	-- If we do not have an active item do not do any processing
	if( activeItemData == nil or db.faction.stats[activeItemData.uniqueID] == nil ) then d( "activeItemData or db.faction.stats[activeItemData.uniqueID] is nil" ) return false end
	-- Sanity check that the item is salvagable by the player
	if( not isItemSalvagableByPlayer( activeItemData ) ) then d( "Item is not salvagable by the player." ) return false end
	
	local backpackType = nil
	
	-- Clear our active slot
	activeSlot 	= nil
	activeLoc	= nil
	
	-- Verify we have one of the items available in the inventory
	local itemSlots = getAllAvailableSlotsForItem( activeItemData )
	for _, item in pairs( itemSlots )
	do
		-- Get the backpack type for the item
		backpackType 	= Crusher.itemLocToBackpackType[item.loc]
		
		-- Attempt to get a lock on the item
		if( EA_BackpackUtilsMediator.RequestLockForSlot( item.slot, backpackType, windowId, {r=0,g=255,b=0} ) ) then
			-- We have a lock lets use this slot
			activeSlot 	= item.slot
			activeLoc	= item.loc
		
			break
		end	
	end
	
	del( itemSlots )
	
	-- If we have an active slot, we have a lock so lets use this slot/loc for the salvage
	if( activeSlot ~= nil and activeLoc ~= nil and db.faction.stats[activeItemData.uniqueID] ~= nil ) then
		-- Execute the salvaging of the item
		if( not RequestSalvageItem( activeSlot, backpackType, GameData.SalvagingTypes.MAGICAL, db.faction.stats[activeItemData.uniqueID] ) ) then
			-- If the request fails, let the user know
			DialogManager.MakeOneButtonDialog( T["Unable to salvage item.  You broke it, you bought it!"], T["Ok"], nil )		
			return false
		end
	end
			
	return true				
end

function setActiveItemData( itemData )
	local texture, textureDx, textureDy = "", 0, 0
	local color	= { r=222, g=192, b=50 }
	
	-- Update our active item data	
	activeItemData = itemData
	
	-- If an item was passed verify the user can handle this item
		-- if they can lookup what information we need  
	if( activeItemData ~= nil ) then
		-- Verify the item is salvagable
		if( isItemSalvagable( activeItemData ) ) then
			-- Verify it is salvagable by the player
			if( isItemSalvagableByPlayer( activeItemData ) ) then
				texture, textureDx, textureDy = GetIconData( activeItemData.iconNum )
				color = DataUtils.GetItemRarityColor( activeItemData )
			else
				TextLogAddEntry( "Chat", SystemData.ChatLogFilters.CRAFTING, GetString( StringTables.Default.TEXT_SALVAGING_FAIL_NOT_ENOUGH_SKILL ) )
				activeItemData = nil				
			end
		else
			TextLogAddEntry( "Chat", SystemData.ChatLogFilters.CRAFTING, GetString( StringTables.Default.TEXT_SALVAGING_ERROR ) )
			activeItemData = nil
		end
	end
	
	-- Update the display items accordingly
	DynamicImageSetTexture( window.W.General.Item_Button.name .. "Icon", texture, textureDx, textureDy )
	WindowSetTintColor( window.W.General.ItemOverlay_Button.name, color.r, color.g, color.b )
	
	-- Update the total item display
	updateTotalItemCountDisplay()
	
	-- Display the stat selection menu if there is no saved stat, other
	-- wise the saved stat becomes the active stat
	if( activeItemData ~= nil and db.faction.stats[activeItemData.uniqueID] == nil ) then
		displayStatSelectionMenu()
	end
end

function updateTotalItemCountDisplay()
	local totalItemCount = L""
	local color				= { r=255, g=255, b=255 }
	if( activeItemData ~= nil and isItemSalvagableByPlayer( activeItemData ) ) then
		totalItemCount = towstring( getTotalItemCount( activeItemData ) )
		local difficultyClass, _ = GetSalvagingDifficulty( activeItemData.iLevel )
		color = CraftingUtils.SalvagingDifficulty[difficultyClass].color
	end
	LabelSetText( window.W.General.Item_Button.name .. "Count", totalItemCount )
	LabelSetTextColor( window.W.General.Item_Button.name .. "Count", color.r, color.g, color.b )
end

function updateUIPosition()
	if( DoesWindowExist( windowId ) ) then
		local anchor = db.profile.anchor
		WindowClearAnchors( windowId )
    	WindowAddAnchor( windowId, anchor.point, anchor.relwin, anchor.relpoint, anchor.x, anchor.y )
    	WindowSetScale( windowId, anchor.scale )
    end
    
    if( DoesWindowExist( iconWindowId ) ) then
    	local icon = db.profile.icon
    	WindowClearAnchors( iconWindowId )
    	WindowAddAnchor( iconWindowId, icon.point, icon.relwin, icon.relpoint, icon.x, icon.y )
    	WindowSetScale( iconWindowId, icon.scale )
    	WindowSetShowing( iconWindowId, icon.visible )
    end
end
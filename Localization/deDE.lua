--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Crusher/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Crusher", "deDE")
if not T then return end

T["(Crusher) use /crusher to bring up the Crusher window."] = L"(Crusher) /crusher nutzen, um das Crusher Fenster zu �ffnen"
T["Apply"] = L"Anwenden"
T["Are you sure?"] = L"Bist du sicher?"
T["Close"] = L"Schlie�en"
T["Copy Settings From:"] = L"Einstellungen kopieren von:"
T["Crusher %s initialized."] = L"Crusher %s initialisiert."
T["Crusher - Configuration"] = L"Crusher Konfiguration"
T["Crusher Toggle"] = L"Crusher Toggle"
T["Current Profile:"] = L"Aktuelles Profil:"
T["Enable Debugging"] = L"Debugging aktivieren"
T["General"] = L"Allgemein"
T["NOTE: Profile changes take effect immediately!"] = L"BEMERKUNG: Profil�nderungen treten sofort in Kraft!"
T["Profiles"] = L"Profile"
T["Revert"] = L"Wiederherstellen"
T["Set Active Profile:"] = L"Aktuelles Profil setzen:"
T["Stop Crushing On Combat"] = L"Kampf stoppt Verwertung"
T["Stop Crushing On Spellcast"] = L"F�higkeit stoppt Verwertung"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNUNG: Mit Bet�tigen des Knopfes wird das aktuelle Profil auf die Standardwerte zur�ckgesetzt!"


--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Crusher/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Crusher", "frFR")
if not T then return end

-- T["(Crusher) use /crusher to bring up the Crusher window."] = L""
-- T["Apply"] = L""
-- T["Are you sure?"] = L""
-- T["Close"] = L""
-- T["Copy Settings From:"] = L""
-- T["Crusher %s initialized."] = L""
-- T["Crusher - Configuration"] = L""
-- T["Crusher Toggle"] = L""
-- T["Current Profile:"] = L""
-- T["Enable Debugging"] = L""
-- T["General"] = L""
-- T["NOTE: Profile changes take effect immediately!"] = L""
-- T["Profiles"] = L""
-- T["Revert"] = L""
-- T["Set Active Profile:"] = L""
-- T["Stop Crushing On Combat"] = L""
-- T["Stop Crushing On Spellcast"] = L""
-- T["WARNING: Clicking this button will restore the current profile back to default values!"] = L""


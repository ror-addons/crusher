--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Crusher/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "Crusher", "enUS", true, debug )

T["(Crusher) use /crusher to bring up the Crusher window."] = L"(Crusher) use /crusher to bring up the Crusher window."
T["Apply"] = L"Apply"
T["Are you sure?"] = L"Are you sure?"
T["Close"] = L"Close"
T["Copy Settings From:"] = L"Copy Settings From:"
T["Crusher %s initialized."] = L"Crusher %s initialized."
T["Crusher - Configuration"] = L"Crusher - Configuration"
T["Crusher Toggle"] = L"Crusher Toggle"
T["Current Profile:"] = L"Current Profile:"
T["Enable Debugging"] = L"Enable Debugging"
T["General"] = L"General"
T["NOTE: Profile changes take effect immediately!"] = L"NOTE: Profile changes take effect immediately!"
T["Profiles"] = L"Profiles"
T["Revert"] = L"Revert"
T["Set Active Profile:"] = L"Set Active Profile:"
T["Stop Crushing On Combat"] = L"Stop Crushing On Combat"
T["Stop Crushing On Spellcast"] = L"Stop Crushing On Spellcast"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNING: Clicking this button will restore the current profile back to default values!"

--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not CrusherConfig then CrusherConfig = {} end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Crusher", debug )

local windowId 					= "CrusherConfig"

local configWindowScroll		= windowId .. "Scroll"
local configWindowScrollChild	= configWindowScroll .. "Child"
local MAX_VISIBLE_ROWS 			= 15

-- Our list of registered windows
local configuredWindows			= {}

local displayOrder 				= {}		
CrusherConfig.displayData 			= {}

local currentDisplayedConfig	= 1

local dbOptions

function CrusherConfig.GetWindowName() return windowId end
function CrusherConfig.IsShowing() return WindowGetShowing( windowId ) end
function CrusherConfig.GetActiveConfigWindowIndex() return currentDisplayedConfig end
function CrusherConfig.GetDBOptions() return dbOptions.handler end

function CrusherConfig.OnLoad( crusherDB, crusherDBOptions )
	-- Store our db options
	dbOptions 	= crusherDBOptions

	-- Set our configuration title
	LabelSetText( windowId .. "TitleBarText", T["Crusher - Configuration"] )
	
	-- Set our highlighting for the listbox
	DataUtils.SetListRowAlternatingTints( windowId .. "List", MAX_VISIBLE_ROWS )
	
	-- Create our window display data
	CrusherConfig.CreateDisplayData()
	
	-- Display the window list
	CrusherConfig.UpdateConfigList()
	
	-- Initialize our configuration windows
	for index, window in ipairs( configuredWindows )
    do
    	-- Create and store our window
    	window:Initialize( crusherDB, dbOptions )
    	
    	-- Hide the config dialog
    	CrusherConfig.HideConfig( index )
    end
    
    -- Display our initial window
    CrusherConfig.UpdateDisplayedConfig( currentDisplayedConfig )
    
    -- Revert all of our windows to set their settings
	CrusherConfig.RevertDialog()
	
	-- Set our close button title
	LabelSetText( windowId .. "Version", towstring( Crusher.GetVersion() ) )
	
	-- Set our close button title
	ButtonSetText( windowId .. "CloseButton", T["Close"] )	 
	
	-- Set our revert button title
	ButtonSetText( windowId .. "RevertButton", T["Revert"] )
	
	-- Set our apply button title
	ButtonSetText( windowId .. "ApplyButton", T["Apply"] )
end

function CrusherConfig.CreateDisplayData()
	-- Clear any existing data
	CrusherConfig.displayData = {}
	
	-- Create our list of configuration pages
	for slotNum, window in pairs( configuredWindows ) 
	do
		local listDisplayItem = {}
		
		listDisplayItem.slotNum 	= slotNum
		listDisplayItem.Name		= window.Name
    
    	CrusherConfig.displayData[slotNum] = listDisplayItem
	end
end

function CrusherConfig.UpdateConfigList()
	-- Clear our current display data
	displayOrder = {}
    
    -- Create the list we will use to display
    for index,_ in ipairs( CrusherConfig.displayData )
    do
    	-- Add this to the end of our display
    	table.insert( displayOrder, index )
    end
    
    -- Display the data
    ListBoxSetDisplayOrder( windowId .. "List", displayOrder )
end

function CrusherConfig.PopulateDisplay()
	local slotNum, config
	 
	for row, data in ipairs( CrusherConfigList.PopulatorIndices ) 
	do
		CrusherConfig.UpdateListRow( CrusherConfig.displayData[data], windowId .. "ListRow".. row )
	end 
end

function CrusherConfig.UpdateListRow( config, rowName )
	if( config.slotNum == currentDisplayedConfig ) then
		LabelSetTextColor( rowName .. "Name", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
	else
		LabelSetTextColor( rowName .. "Name", 255 ,255 ,255 )
	end
end

function CrusherConfig.OnLButtonUpConfigList()
	local slotNumber, rowNumber, config = CrusherConfig.GetSlotRowNumForActiveListRow()
	
	if( rowNumber ~= currentDisplayedConfig ) then
		CrusherConfig.UpdateDisplayedConfig( rowNumber )	
	end
end

function CrusherConfig.UpdateDisplayedConfig( newConfig )
	local prevWindow = configWindowScrollChild
	
	-- Hide the old config
	CrusherConfig.HideConfig( currentDisplayedConfig )
	
	-- Clear the old config parent
	CrusherConfig.SetConfigParent( currentDisplayedConfig, "Root" )
	
	-- Set the new config parent
	CrusherConfig.SetConfigParent( newConfig, configWindowScrollChild )
	
	-- Anchor the configs children
	CrusherConfig.ReanchorConfigDisplay( newConfig, configWindowScrollChild )
	
	-- Display the new config
	CrusherConfig.ShowConfig( newConfig )
	
	-- Update our current displayed config
	currentDisplayedConfig = newConfig
	
	-- Tell our scroll window to update its scroll window
	ScrollWindowSetOffset( configWindowScroll, 0 )
	ScrollWindowUpdateScrollRect( configWindowScroll )

	-- Repopulate our display
	CrusherConfig.PopulateDisplay()
end

function CrusherConfig.GetSlotRowNumForActiveListRow()
	local rowNumber, slowNumber, config
	
	-- Get the row within the window
	rowNumber = WindowGetId( SystemData.ActiveWindow.name ) 

	-- Get the data index from the list box
    local dataIndex = ListBoxGetDataIndex( windowId .. "List" , rowNumber )
    
    -- Get the slot from the data
    if( dataIndex ~= nil ) then
    	
    	slotNumber = CrusherConfig.displayData[dataIndex].slotNum
    
	    -- Get the data
	    if( slotNumber ~= nil ) then
	    	config = configuredWindows[slotNumber]
	    end
	end
    
	return slotNumber, rowNumber, config
end

function CrusherConfig.SaveDialog()
	-- Revert all of our windows to set their settings
	for index, window in ipairs( configuredWindows )
    do
    	window:Apply()
    end
end

function CrusherConfig.RevertDialog()
	-- Revert all of our windows to set their settings
	for index, window in ipairs( configuredWindows )
    do
    	window:Revert()
    end
end

function CrusherConfig.OnClose()
	WindowSetShowing( windowId, false )
end

function CrusherConfig.OnApply()
	-- Save the dialog
	CrusherConfig.SaveDialog()
end

function CrusherConfig.OnRevert()
	CrusherConfig.RevertDialog()
end

function CrusherConfig.RegisterWindow( window )
	table.insert( configuredWindows, window )
	return #configuredWindows
end

function CrusherConfig.OnResizeBegin()
    WindowUtils.BeginResize( windowId, "topleft", 800, 500, CrusherConfig.OnResizeEnd )
end

function CrusherConfig.OnResizeEnd()
	CrusherConfig.UpdateDisplayedConfig( currentDisplayedConfig )
end

function CrusherConfig.ShowConfig( index )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Show()
		end
	end
end

function CrusherConfig.HideConfig( index )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Hide()
		end
	end
end

function CrusherConfig.SetConfigParent( index, parent )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Parent( parent )
		end
	end
end

function CrusherConfig.ReanchorConfigDisplay( index, initialParent )
	local prevWindow	= initialParent
	local leftAnchor 	= "topleft"
	local rightAnchor 	= "topright"

	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:ClearAnchors()
			v:AddAnchor( prevWindow, leftAnchor, "topleft", 0, 0 )
			v:AddAnchor( prevWindow, rightAnchor, "topright", 0, 0 )
			
			prevWindow = v.name
			leftAnchor = "bottomleft"
			rightAnchor = "bottomright"
		end
	end
end

function CrusherConfig.UpdateColorSelection( currentValue, slider, text )
	local changed 		= false
	local sliderColor 	= math.floor( slider:GetValue() )
	local textColor 	= tonumber( text:GetText() ) or sliderColor
	
	if( sliderColor ~= currentValue ) then
		currentValue = sliderColor
		text:SetText( towstring( currentValue ) )
		changed = true
	elseif( textColor ~= currentValue ) then
		currentValue = textColor
		slider:SetValue( currentValue )
		changed = true
	end
		
	return changed, currentValue
end

function CrusherConfig.UpdateAlphaSelection( currentValue, slider, text )
	local changed 		= false
	local sliderColor 	= slider:GetValue()
	local textColor 	= tonumber( text:GetText() ) or sliderColor
	
	if( sliderColor ~= currentValue ) then
		currentValue = sliderColor
		text:SetText( wstring.format( L"%.2f", towstring( currentValue ) ) )
		changed = true
	elseif( textColor ~= currentValue ) then
		currentValue = textColor
		slider:SetValue( currentValue )
		changed = true
	end
	
	return changed, currentValue
end
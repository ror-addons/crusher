--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not CrusherConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Crusher", debug )
local LibGUI 	= LibStub( "LibGUI" )

local initialized		= false

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings
local window	= {
	Name		= T["General"],
	display		= {},
	W			= {},
}

local db

function InitializeWindow( self, crusherDB, crusherDBOptions )
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e
	
	-- Store our db here
	db 			= crusherDB
	
	-- General Window
	w = LibGUI( "window", nil, "CrusherWindowDefault" )
	w:ClearAnchors()
	w:Resize( 400, 380 )
	w:Show( true )
	window.W.General = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	
	--
	-- GENERAL WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["General"] )
    window.W.General.Title_Label = e
    
    -- General - Stop Crafting On Combat Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 10, 5 )
		window.W.General.StopCraftingOnCombat_Checkbox = e
    
    -- General - Stop Crafting On Combat Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.StopCraftingOnCombat_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Stop Crushing On Combat"] )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, 
				T["With this option enabled you will stop crafting if you go into combat."] )
	        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    window.W.General.StopCraftingOnCombat_Label = e
    
    -- General - Stop Crafting On Spellcast Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.StopCraftingOnCombat_Checkbox, "bottomleft", "topleft", 0, 5 )
	window.W.General.StopCraftingOnSpellcast_Checkbox = e
    
    -- General - Stop Crafting On Spellcast Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.StopCraftingOnSpellcast_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Stop Crushing On Spellcast"] )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, 
				T["With this option enabled you will stop crafting if you cast a spell."] )
	        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    window.W.General.StopCraftingOnSpellcast_Label = e
    
    -- General - Enable Debugging Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General, "bottomleft", "bottomleft", 25, -5 )
	window.W.General.EnableDebugging_Checkbox = e
    
    -- General - Enable Debugging Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.EnableDebugging_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Debugging"] )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, 
				T["With this enabled Crusher will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] )
	        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    window.W.General.EnableDebugging_Label = e
    
   	initialized = true
end

function ApplyConfigSettings()
	db.profile.general.stopincombat					= window.W.General.StopCraftingOnCombat_Checkbox:GetValue()
	db.profile.general.stoponspellcast				= window.W.General.StopCraftingOnSpellcast_Checkbox:GetValue()
	db.profile.general.debug 						= window.W.General.EnableDebugging_Checkbox:GetValue()
end

function RevertConfigSettings()
	window.W.General.StopCraftingOnCombat_Checkbox		: SetValue( db.profile.general.stopincombat )
	window.W.General.StopCraftingOnSpellcast_Checkbox	: SetValue( db.profile.general.stoponspellcast )
	window.W.General.EnableDebugging_Checkbox			: SetValue( db.profile.general.debug )
end

window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= CrusherConfig.RegisterWindow( window )